This document was originally created by a member of the Boston
CryptoParty for a discussion in November 2016. 

I've converted the original LaTeX document to Markdown to improve the
editability for a wider audience. 
